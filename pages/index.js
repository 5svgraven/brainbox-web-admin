import { Container, Row, Col } from "react-bootstrap";
import Head from "next/head";
import Login from "../lib/component/login";

export default function Index() {
  return (
    <>
      <Head>
        <title>Brainbox - Login</title>
      </Head>
      <Login />
    </>
  );
}
