import React from "react";
import Review from "../../lib/component/review";
import Tutorial from "../../lib/component/tutorial";
import { Tab, Row, Col, Nav, Navbar } from "react-bootstrap";
import Router from "next/router";
import { resetStorage } from "../../lib/services/persistence_service";
import Head from "next/head";

export default function Dashboard() {
  const logout = () => {
    resetStorage();
    Router.push("/");
  };

  return (
    <>
      <Head>
        <title>Dashboard</title>
      </Head>
      <Navbar bg="primary" variant="dark" style={{ marginBottom: 10 }}>
        <Navbar.Brand href="/dashboard" style={{ marginLeft: 10 }}>
          Brainbox
        </Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link href="/dashboard">Home</Nav.Link>
        </Nav>
        <Nav>
          <Nav.Link href="/" style={{ marginRight: 10 }}>
            Logout
          </Nav.Link>
        </Nav>
      </Navbar>
      <Tab.Container id="left-tabs-example" defaultActiveKey="first">
        <Row>
          <Col sm={2}>
            <Nav variant="pills" className="flex-column">
              <Nav.Item>
                <Nav.Link eventKey="first">Review</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="second">Tutorial</Nav.Link>
              </Nav.Item>
            </Nav>
          </Col>
          <Col sm={10}>
            <Tab.Content>
              <Tab.Pane eventKey="first">
                <Review />
              </Tab.Pane>
              <Tab.Pane eventKey="second">
                <Tutorial />
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    </>
  );
}
