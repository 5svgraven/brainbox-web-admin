import React from "react";
import { Card, Button, Row } from "react-bootstrap";

export default function ReviewCard(props) {
  return (
    <Card style={{ width: "18rem", margin: 20 }}>
      <Card.Body>
        <Card.Title>{props.title}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">
          {props.typeOfReviewer}
        </Card.Subtitle>
        <strong>Date Period</strong>
        <Card.Text>
          {props.startDate} - {props.endDate}
        </Card.Text>
        <strong>Time Period</strong>
        <Card.Text>
          {props.amStartTime} - {props.amEndTime}
        </Card.Text>
        <Card.Text>
          {props.pmStartTime} - {props.pmEndTime}
        </Card.Text>
        <Card.Text>
          {props.enrollees}/{props.maxTutee} Students
        </Card.Text>
        <Card.Text>Price: {props.price}</Card.Text>
        <Row>
          <Button onClick={props.positiveAction}>Update Review</Button>
          <Button
            variant="danger"
            style={{ marginTop: 10 }}
            onClick={props.negativeAction}
          >
            Delete Review
          </Button>
        </Row>
      </Card.Body>
    </Card>
  );
}
