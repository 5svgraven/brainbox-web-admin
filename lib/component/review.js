import { useState, useEffect } from "react";
import { Button, Row, Modal, Col } from "react-bootstrap";
import ReviewService from "../services/review_service";
import ReviewCard from "./review_card";
import {
  Fab,
  TextField,
  FormControl,
  Select,
  InputLabel,
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  DatePicker,
} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import moment from "moment";
import swal from "sweetalert";

const Review = () => {
  const reviewService = new ReviewService();
  const [reset, setReset] = useState(false);
  const stateReset = () => {
    setReset(!reset);
  };
  const [listOfReview, setListOfReview] = useState([]);
  const [showAddNew, setShowAddNew] = useState(false);
  const [showUpdate, setShowUpdate] = useState(false);

  // post data
  const [reviewId, setReviewId] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState();
  const [typeOfReviewer, setTypeOfReviewer] = useState("");
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [amStartTime, setAmStartTime] = useState(new Date());
  const [amEndTime, setAmEndTime] = useState(new Date());
  const [pmStartTime, setPmStartTime] = useState(new Date());
  const [pmEndTime, setPmEndTime] = useState(new Date());
  const [maxTutee, setMaxTutee] = useState();
  // end of post data

  useEffect(() => {
    review();
  }, [reset]);

  const review = async () => {
    const data = await reviewService.getAllReview();
    setListOfReview(data);
  };

  const showReviews = listOfReview.map((review, index) => {
    return (
      <ReviewCard
        key={index}
        title={review.name}
        typeOfReviewer={review.typeOfReviewer}
        startDate={review.startDate}
        endDate={review.endDate}
        amStartTime={review.amStartTime}
        amEndTime={review.amEndTime}
        pmStartTime={review.pmStartTime}
        pmEndTime={review.pmEndTime}
        enrollees={review.enrollees.length}
        maxTutee={review.maxTutee}
        price={review.price}
        positiveAction={() => {
          setShowUpdate(true);
          handleUpdate(review);
        }}
        negativeAction={() => handleDelete(review.id)}
      />
    );
  });

  const handleUpdate = (review) => {
    setReviewId(review.id);
    setName(review.name);
    setDescription(review.description);
    setStartDate(new Date(review.startDate));
    setEndDate(new Date(review.endDate));
    setAmStartTime(null);
    setAmEndTime(null);
    setPmStartTime(null);
    setPmEndTime(null);
    setTypeOfReviewer(review.typeOfReviewer);
    setMaxTutee(review.maxTutee);
    setPrice(review.price);
    setShowAddNew(true);
  };

  const handleUpdateButton = async () => {
    const result = await reviewService.updateReview(
      reviewId,
      name,
      description,
      price,
      typeOfReviewer,
      moment(startDate).format("yy-MM-DD"),
      moment(endDate).format("yy-MM-DD"),
      moment(amStartTime).format("h:mm a"),
      moment(amEndTime).format("h:mm a"),
      moment(pmStartTime).format("h:mm a"),
      moment(pmEndTime).format("h:mm a"),
      maxTutee
    );
    if (result) {
      swal("Updated Successfully", { icon: "success" });
      handleCloseAddModal();
    }
  };

  const handleAddReview = async () => {
    console.log(moment(startDate).format("yy-MM-DD"));
    console.log(moment(amStartTime).format("h:mm a"));
    const result = await reviewService.createReview(
      name,
      description,
      price,
      typeOfReviewer,
      moment(startDate).format("yy-MM-DD"),
      moment(endDate).format("yy-MM-DD"),
      moment(amStartTime).format("h:mm a"),
      moment(amEndTime).format("h:mm a"),
      moment(pmStartTime).format("h:mm a"),
      moment(pmEndTime).format("h:mm a"),
      maxTutee
    );
    if (result) {
      swal("Added Successfully", { icon: "success" });
      handleCloseAddModal();
    }
  };

  const handleDelete = async (reviewId) => {
    swal({
      title: "Delete Review?",
      text: "Are you sure you want to continue?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: "Yes",
      },
    }).then(async (success) => {
      if (success) {
        const result = await reviewService.deleteReview(reviewId);
        if (result) {
          swal("Deleted Successfully", { icon: "success" });
          handleCloseAddModal();
        } else {
          swal("Something went wrong", { icon: "danger" });
        }
      } else {
        swal("Cancelled", { icon: "info" });
      }
    });
  };

  const handleCloseAddModal = () => {
    setShowAddNew(false);
    setName("");
    setDescription("");
    setPrice(null);
    setStartDate(Date.now());
    setEndDate(Date.now());
    setTypeOfReviewer("");
    setMaxTutee(null);
    stateReset();
  };

  const handleAddNew = () => {
    handleCloseAddModal();
    setShowAddNew(true);
    setShowUpdate(false);
  };

  return (
    <>
      <h1>Review</h1>
      <Fab color="primary" variant="extended" onClick={handleAddNew}>
        Add New
      </Fab>

      <Row>{showReviews}</Row>
      <Modal
        show={showAddNew}
        onHide={() => setShowAddNew(false)}
        centered
        size="md"
      >
        <Modal.Header closeButton>
          <Modal.Title>Add New Reviewer</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <FormControl variant="outlined" fullWidth style={{ marginBottom: 5 }}>
            <InputLabel>Name</InputLabel>
            <Select
              native
              value={name}
              onChange={(e) => setName(e.target.value)}
              label="Name"
              defaultValue="-"
              inputProps={{
                name: "Name",
              }}
            >
              <option aria-label=" - " value="" />
              <option>UPCAT Review</option>
              <option>Civil Service Exam Review</option>
              <option>PSHS-NCE Review</option>
            </Select>
          </FormControl>
          <TextField
            fullWidth
            size="medium"
            value={description}
            type="text"
            onChange={(e) => setDescription(e.target.value)}
            label="Description"
            variant="outlined"
            margin="dense"
          />
          <TextField
            style={{ marginBottom: 15 }}
            fullWidth
            size="medium"
            value={price}
            type="Number"
            onChange={(e) => setPrice(e.target.value)}
            label="Price"
            variant="outlined"
            margin="dense"
          />
          <FormControl variant="outlined" fullWidth>
            <InputLabel>Type of Reviewer</InputLabel>
            <Select
              native
              value={typeOfReviewer}
              onChange={(e) => setTypeOfReviewer(e.target.value)}
              label="Type of Reviewer"
              inputProps={{
                name: "Type of Reviewer",
              }}
            >
              <option aria-label=" - " value="" />
              <option>OneOnOne</option>
              <option>Group</option>
            </Select>
          </FormControl>
          <MuiPickersUtilsProvider utils={MomentUtils} libInstance={moment}>
            <Row
              className="d-flex justify-content-between"
              style={{ marginTop: 20 }}
            >
              <Col>
                <DatePicker
                  id="startDate"
                  autoOk
                  label="Start Date"
                  clearable
                  format="MMMM DD, yyyy"
                  value={startDate}
                  onChange={(date) => setStartDate(date)}
                  variant="outlined"
                />
              </Col>
              <Col>
                <DatePicker
                  autoOk
                  id="endDate"
                  label="End Date"
                  clearable
                  format="MMMM DD, yyyy"
                  value={endDate}
                  onChange={(date) => setEndDate(date)}
                  variant="outlined"
                />
              </Col>
            </Row>
            <Row className="d-flex justify-content-between">
              <Col>
                <KeyboardTimePicker
                  id="amStartTime"
                  margin="dense"
                  label="AM Start Time"
                  value={amStartTime}
                  onChange={(time) => setAmStartTime(time)}
                  KeyboardButtonProps={{
                    "aria-label": "change time",
                  }}
                />
              </Col>
              <Col>
                <KeyboardTimePicker
                  id="amEndTime"
                  margin="dense"
                  label="AM End Time"
                  value={amEndTime}
                  onChange={(time) => setAmEndTime(time)}
                  KeyboardButtonProps={{
                    "aria-label": "change time",
                  }}
                />
              </Col>
            </Row>

            <Row
              className="d-flex justify-content-between"
              style={{ marginBottom: 10 }}
            >
              <Col>
                <KeyboardTimePicker
                  id="pmStartTime"
                  margin="dense"
                  label="PM Start Time"
                  value={pmStartTime}
                  onChange={(time) => setPmStartTime(time)}
                  KeyboardButtonProps={{
                    "aria-label": "change time",
                  }}
                />
              </Col>
              <Col>
                <KeyboardTimePicker
                  margin="dense"
                  id="pmEndTime"
                  label="PM End Time"
                  value={pmEndTime}
                  onChange={(time) => setPmEndTime(time)}
                  KeyboardButtonProps={{
                    "aria-label": "change time",
                  }}
                />
              </Col>
            </Row>
          </MuiPickersUtilsProvider>
          <TextField
            fullWidth
            size="medium"
            value={maxTutee}
            type="Number"
            onChange={(e) => setMaxTutee(e.target.value)}
            label="Max Tutee"
            variant="outlined"
            margin="dense"
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => handleCloseAddModal()}>
            Cancel
          </Button>
          {showUpdate ? (
            <Button variant="primary" onClick={handleUpdateButton}>
              Update Review
            </Button>
          ) : (
            <Button variant="primary" onClick={handleAddReview}>
              Add Review
            </Button>
          )}
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default Review;
