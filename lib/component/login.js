import { useState } from "react";
import { Row, Col, Card } from "react-bootstrap";
import { TextField, Button } from "@material-ui/core";
import AuthService from "../services/auth_service";
import { saveString } from "../services/persistence_service";
import Router from "next/router";

import React from "react";
import Avatar from "@material-ui/core/Avatar";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

const Login = () => {
  const authService = new AuthService();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorState, setErrorState] = useState(false);
  const helperText = "Username or Password is incorrect";

  const login = async () => {
    const result = await authService.login(username, password);
    if (!result) {
      setErrorState(true);
    } else {
      saveString("token", result.access);
      dispose();
      Router.push("/dashboard");
    }
  };

  const dispose = () => {
    setErrorState(false);
    setUsername("");
    setPassword("");
  };

  function Copyright() {
    return (
      <Typography variant="body2" color="textSecondary" align="center">
        {"Copyright © "}
        <Link
          color="inherit"
          target="_"
          href="https://web.facebook.com/brainboxtutorialandreviewcenter"
        >
          Brainbox
        </Link>{" "}
        {new Date().getFullYear()}
        {"."}
      </Typography>
    );
  }

  return (
    <>
      <Row className="d-flex justify-content-center">
        <Card
          className="text-center mt-10"
          style={{ width: "20em", marginTop: 30 }}
        >
          <Card.Body>
            <Card.Title>Login</Card.Title>
            <Col>
              <TextField
                fullWidth
                size="medium"
                value={username}
                type="text"
                onChange={(e) => setUsername(e.target.value)}
                placeholder="Username"
                variant="outlined"
                margin="dense"
                error={errorState}
              />
            </Col>
            <Col>
              <TextField
                fullWidth
                size="medium"
                value={password}
                type="password"
                onChange={(e) => setPassword(e.target.value)}
                placeholder="Password"
                variant="outlined"
                margin="dense"
                helperText={errorState ? helperText : ""}
                error={errorState}
              />
            </Col>
            <Button
              fullWidth
              style={{ marginTop: 20 }}
              variant="contained"
              color="primary"
              onClick={login}
            >
              Login
            </Button>
          </Card.Body>
          <Card.Footer>{Copyright()}</Card.Footer>
        </Card>
      </Row>
    </>
  );
};

export default Login;
