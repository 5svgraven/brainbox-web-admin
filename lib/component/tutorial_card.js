import moment from "moment";
import React from "react";
import { useState } from "react";
import { Card, Button, Row } from "react-bootstrap";

export default function TutorialCard(props) {
  const [title, setTitle] = useState(props.title);
  const [price, setPrice] = useState(props.price);
  const [startDate, setStartDate] = useState(props.startDate);
  const [endDate, setEndDate] = useState(props.endDate);
  const [type, setType] = useState(props.type);
  const [maxStudent, setMaxStudent] = useState(props.maxStudent);
  const [tutorName, setTutorName] = useState(props.tutor);
  const [enrolleesLength, setEnrolleesLength] = useState(props.enrolleesLength);

  return (
    <Card style={{ width: "18rem", margin: 20 }}>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">{type}</Card.Subtitle>
        <strong>Tutor</strong>
        <Card.Text>{tutorName}</Card.Text>
        <strong>Date Period</strong>
        <Card.Text>
          {moment(startDate).format("MMMM DD, yyyy")} -{" "}
          {moment(endDate).format("MMMM DD, yyyy")}
        </Card.Text>
        <Card.Text>
          {enrolleesLength}/{maxStudent} Students
        </Card.Text>
        <Card.Text>Price: {price}</Card.Text>
        <Row>
          <Button onClick={props.positiveAction}>Update Tutorial</Button>
          <Button
            variant="danger"
            style={{ marginTop: 10 }}
            onClick={props.negativeAction}
          >
            Delete Tutorial
          </Button>
        </Row>
      </Card.Body>
    </Card>
  );
}
