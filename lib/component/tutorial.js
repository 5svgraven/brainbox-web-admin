import { useState, useEffect } from "react";
import { Button, Row, Modal, Col } from "react-bootstrap";
import TutorialService from "../services/tutorial_service";
import TutorialCard from "./tutorial_card";
import {
  Fab,
  TextField,
  FormControl,
  Select,
  InputLabel,
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  DatePicker,
} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import moment from "moment";
import swal from "sweetalert";

const Tutorial = () => {
  const tutorialService = new TutorialService();
  const [listOfTutorial, setListOfTutorial] = useState([]);
  const [state, setState] = useState(false);
  const stateReset = () => {
    setState(!state);
  };
  const [showAddNew, setShowAddNew] = useState(false);
  // data
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState(null);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [type, setType] = useState("");
  const [maxStudent, setMaxStudent] = useState(null);
  const [enrolleesLength, setEnrolleesLength] = useState(null);
  const [tutorName, setTutorName] = useState("");
  const [showTutor, setShowTutor] = useState(false);
  // end data
  useEffect(() => {
    getTutorials();
  }, [state]);

  const getTutorials = async () => {
    const data = await tutorialService.getAllTutorial();
    console.log(data);
    setListOfTutorial(data);
  };

  const showTutorials = listOfTutorial.map((tutorial, index) => {
    return (
      <TutorialCard
        key={index}
        title={tutorial.subject}
        price={tutorial.price}
        startDate={tutorial.startDate}
        endDate={tutorial.endDate}
        type={tutorial.type}
        maxStudent={tutorial.maxStudents}
        tutorName="Raven Rama"
        enrolleesLength={tutorial.enrollees.length}
      />
    );
  });

  return (
    <>
      <h1>Tutorial</h1>
      <Fab
        color="primary"
        variant="extended"
        onClick={() => {
          setShowAddNew(true);
        }}
      >
        Add New
      </Fab>
      <Row>{showTutorials}</Row>
      <Modal
        show={showAddNew}
        onHide={() => setShowAddNew(false)}
        centered
        size="md"
      >
        <Modal.Header closeButton>
          <Modal.Title>Add New Tutorial</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <TextField
            fullWidth
            size="medium"
            value={title}
            type="text"
            onChange={(e) => setTitle(e.target.value)}
            label="Title"
            variant="outlined"
            margin="dense"
          />
          <TextField
            fullWidth
            size="medium"
            value={type}
            type="text"
            onChange={(e) => setType(e.target.value)}
            label="Type"
            variant="outlined"
            margin="dense"
          />
          {showTutor ? (
            <TextField
              fullWidth
              size="medium"
              value={tutorName}
              type="text"
              onChange={(e) => setTutorName(e.target.value)}
              label="Tutor"
              variant="outlined"
              margin="dense"
            />
          ) : (
            <></>
          )}
          <MuiPickersUtilsProvider utils={MomentUtils} libInstance={moment}>
            <Row
              className="d-flex justify-content-between"
              style={{ marginTop: 20, marginBottom: 20 }}
            >
              <Col>
                <DatePicker
                  id="startDate"
                  autoOk
                  label="Start Date"
                  clearable
                  format="MMMM DD, yyyy"
                  value={startDate}
                  onChange={(date) => setStartDate(date)}
                  variant="outlined"
                />
              </Col>
              <Col>
                <DatePicker
                  autoOk
                  id="endDate"
                  label="End Date"
                  clearable
                  format="MMMM DD, yyyy"
                  value={endDate}
                  onChange={(date) => setEndDate(date)}
                  variant="outlined"
                />
              </Col>
            </Row>
          </MuiPickersUtilsProvider>
          <TextField
            fullWidth
            size="medium"
            value={maxStudent}
            type="Number"
            onChange={(e) => setMaxStudent(e.target.value)}
            label="Max Students"
            variant="outlined"
            margin="dense"
          />
          <TextField
            fullWidth
            size="medium"
            value={price}
            type="Number"
            onChange={(e) => setPrice(e.target.value)}
            label="Price"
            variant="outlined"
            margin="dense"
          />
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Tutorial;
