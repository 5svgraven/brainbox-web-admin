export async function saveString(key, value) {
  sessionStorage.setItem(key, value);
}

export async function getString(key) {
  const result = sessionStorage.getItem(key);
  return result;
}

export function resetStorage() {
  sessionStorage.clear();
}
