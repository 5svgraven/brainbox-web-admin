export default class AuthService {
  static get baseUrl() {
    return baseUrl;
  }
  async login(username, password) {
    const res = await fetch(`${baseUrl}/admin/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    });
    const data = await res.json();
    return data;
  }
}

export const baseUrl = "https://api-brainbox.herokuapp.com/api";
