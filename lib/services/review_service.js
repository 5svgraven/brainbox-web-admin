export default class ReviewService {
  static get baseUrl() {
    return baseUrl;
  }
  async login(username, password) {
    const res = await fetch(`${baseUrl}/admin/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    });
    const data = await res.json();
    return data;
  }

  async createReview(
    name,
    description,
    price,
    typeOfReviewer,
    startDate,
    endDate,
    amStartTime,
    amEndTime,
    pmStartTime,
    pmEndTime,
    maxTutee
  ) {
    const res = await fetch(`${baseUrl}/reviewers`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${sessionStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        typeOfReviewer: typeOfReviewer,
        startDate: startDate,
        endDate: endDate,
        amStartTime: amStartTime,
        amEndTime: amEndTime,
        pmStartTime: pmStartTime,
        pmEndTime: pmEndTime,
        maxTutee: maxTutee,
      }),
    });
    const data = await res.json();
    return data;
  }

  async getAllReview() {
    const res = await fetch(`${baseUrl}/reviewers`, {
      headers: {
        Authorization: `Bearer ${sessionStorage.getItem("token")}`,
      },
    });
    const data = await res.json();
    return data;
  }

  async updateReview(
    reviewId,
    name,
    description,
    price,
    typeOfReviewer,
    startDate,
    endDate,
    amStartTime,
    amEndTime,
    pmStartTime,
    pmEndTime,
    maxTutee
  ) {
    const res = await fetch(`${baseUrl}/reviewers`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${sessionStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        reviewId: reviewId,
        name: name,
        description: description,
        price: price,
        typeOfReviewer: typeOfReviewer,
        startDate: startDate,
        endDate: endDate,
        amStartTime: amStartTime,
        amEndTime: amEndTime,
        pmStartTime: pmStartTime,
        pmEndTime: pmEndTime,
        maxTutee: maxTutee,
      }),
    });
    const data = await res.json();
    return data;
  }

  async deleteReview(reviewId) {
    const res = await fetch(`${baseUrl}/reviewers/delete/${reviewId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${sessionStorage.getItem("token")}`,
      },
    });
    const data = await res.json();
    return data;
  }
}

export const baseUrl = "https://api-brainbox.herokuapp.com/api";
