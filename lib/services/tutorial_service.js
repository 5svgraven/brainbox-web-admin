export default class TutorialService {
  static get baseUrl() {
    return baseUrl;
  }
  async login(username, password) {
    const res = await fetch(`${baseUrl}/admin/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    });
    const data = await res.json();
    return data;
  }

  async getAllTutorial() {
    const res = await fetch(`${baseUrl}/tutorial/active`, {
      headers: {
        Authorization: `Bearer ${sessionStorage.getItem("token")}`,
      },
    });
    const data = await res.json();
    return data;
  }
  async getOneTutorial() {}
  async createTutorial() {}
  async updateTutorial() {}
  async deleteTutorial() {}
}

export const baseUrl = "https://api-brainbox.herokuapp.com/api";
